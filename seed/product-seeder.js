const Product = require('../models/product');
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/SewaAja', {useNewUrlParser : true})
    .then(() => console.log('Connected to database'))
    .catch(err => console.error('Could not connect to MongoDB...', err))

const products = [
    new Product({
        imagePath: "https://s3-ap-southeast-1.amazonaws.com/sewa/kamera/1.jpg",
        name: 'Camera Cannon',
        category: 'Kamera',
        description: 'Kamera Bagus',
        price: 150.000
    }),
    new Product({
        imagePath: "https://s3-ap-southeast-1.amazonaws.com/sewa/kamera/2.jpg",
        name: 'Camera Cannon',
        category: 'Kamera',
        description: 'Kamera Bagus',
        price: 150.000
    }),
    new Product({
        imagePath: "https://s3-ap-southeast-1.amazonaws.com/sewa/kamera/3.jpg",
        name: 'Camera Cannon',
        category: 'Kamera',
        description: 'Kamera Bagus',
        price: 150.000
    }),
    new Product({
        imagePath: "https://s3-ap-southeast-1.amazonaws.com/sewa/kamera/4.jpg",
        name: 'Camera Cannon',
        category: 'Kamera',
        description: 'Kamera Bagus',
        price: 150.000
    }),
    new Product({
        imagePath: "https://s3-ap-southeast-1.amazonaws.com/sewa/kamera/5.jpg",
        name: 'Camera Cannon',
        category: 'Kamera',
        description: 'Kamera Bagus',
        price: 150.000
    }),
    new Product({
        imagePath: "https://s3-ap-southeast-1.amazonaws.com/sewa/kamera/6.jpg",
        name: 'Camera Cannon',
        category: 'Kamera',
        description: 'Kamera Bagus',
        price: 150.000
    }),
    new Product({
        imagePath: "https://s3-ap-southeast-1.amazonaws.com/sewa/buku/1.jpg",
        name: 'Rayhan & Angela',
        category: 'Buku',
        description: 'Kamera Bagus',
        price: 150.000
    }),
    new Product({
        imagePath: "https://s3-ap-southeast-1.amazonaws.com/sewa/buku/2.jpg",
        name: 'Rayhan & Angela',
        category: 'Buku',
        description: 'Kamera Bagus',
        price: 150.000
    }),
    new Product({
        imagePath: "https://s3-ap-southeast-1.amazonaws.com/sewa/buku/3.jpg",
        name: 'Rayhan & Angela',
        category: 'Buku',
        description: 'Kamera Bagus',
        price: 150.000
    }),

]

let done = 0;
for(let i = 0; i < products.length; i++){
    products[i].save((err,result) => {
        done++;
        if(done === products.length){
            exit();
        }
    });
}

function exit(){
    mongoose.disconnect();
}