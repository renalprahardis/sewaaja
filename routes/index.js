const express = require('express');
const router = express.Router();
const {ensureAuthenticated } = require('../config/auth');
const Product = require('../models/Product'); 
var Cart = require('../models/cart');

// Welcome Page
router.get('/', (req,res, next) => {
    res.render('welcome');
    
});

//Dashboard
router.get('/dashboard',(req, res, next) => 
    Product.find((err, docs) => {
        let productChunks = [];
        let chunkSize = 3;
        for(let i = 0; i < docs.length; i += chunkSize){
            productChunks.push(docs.slice(i, i + chunkSize));
        }
        res.render('products/index', { title: 'SewaAja', products: docs});
    })
);

router.get('/keranjang/:id', (req, res, next) => {
    let productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    Product.findById(productId, (err, product) => {
        if(err) {
            return res.redirect('/');
        }
        cart.add(product, product.id);
        req.session.cart = cart;
        console.log(req.session.cart);
        res.redirect('/');
    });
});

module.exports = router;
