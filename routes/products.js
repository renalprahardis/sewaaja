const express = require('express');
const router = express.Router();
const Product = require('../models/Product')

router.get('/kamera', (req,res) => {
    res.render('../views/kategori/kamera.ejs');
});


async function getCamera(){
    const career = await Product
        .find({category: 'Kamera'});
    console.log(career);
}

module.exports = router;